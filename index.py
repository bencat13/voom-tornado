import tornado.ioloop
import tornado.web
import os
import pymongo

class ActionHandler(tornado.web.RequestHandler):
    def get(self):
        mydict = self.request.arguments
        x = mycol.insert_one(mydict)
        print(x.inserted_id)
        for y in mycol.find():
            print(y)

class MainHandler(tornado.web.RequestHandler):
     def get(self):
        self.render("index.html", title="Insurance Calculator")
        

def make_app():
    STATIC_DIRNAME = "resources"
    settings = {
    "static_path": os.path.join(os.path.dirname(__file__), STATIC_DIRNAME),
}
    return tornado.web.Application([
        (r"/", MainHandler),
        (r"/log_rate/", ActionHandler)
        
    ],**settings)
    
if __name__ == "__main__":
    app = make_app()
    app.listen(8885)

    myclient = pymongo.MongoClient("mongodb://localhost:27017/")
    mydb = myclient["mydatabase"]
    mycol = mydb["entries"]

   


tornado.ioloop.IOLoop.current().start()
    


    