function getWeather() {
    var lat=32.0853;
    var lng=34.7818;
    this.WeatherData = {
    main : {},
    isDay: true}
    let string = "http://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lng + "&appid=2c24b3ceeea6dee8fc730af0b86b540a"
    fetch(string)
     .then(response=>response.json())
     .then(data=>{setWeather(data);})
    }
    
function setWeather(data){
    var WeatherData = data;
    var url= 'http://openweathermap.org/img/wn/'
    console.log(data)
    var weatherUrl = (url.concat(data.weather[0].icon, '@2x.png'));
    document.getElementById('weather-iconid').src=weatherUrl;
    document.getElementById('temp-weather').innerHTML = (WeatherData.main.temp - 273.15).toFixed(0) + "°C"
    WeatherData.temp_min = (WeatherData.main.temp_min - 273.15).toFixed(0);
    WeatherData.temp_max = (WeatherData.main.temp_max - 273.15).toFixed(0);
    var minmax = WeatherData.temp_min + "°C / " + WeatherData.temp_max + "°C"
    document.getElementById('temp-highlow').innerHTML = minmax
    document.getElementById('temp-feelslike').innerHTML = WeatherData.temp_feels_like = "Feels like: " + (WeatherData.main.feels_like - 273.15).toFixed(0) + "°C";
    document.getElementById('city').innerHTML = data.name
    document.getElementById('humidity').innerHTML = "Humidity: " + data.main.humidity + "%"
}
window.onload = getWeather();